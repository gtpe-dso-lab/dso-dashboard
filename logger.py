"""Create logger."""

import logging
import logging.handlers


def create_logger() -> None:
    """Create logger."""
    # create console handler
    console_handler = logging.StreamHandler()
    # console_handler.setLevel(logging.INFO)

    # create formatter and add it to the handlers
    console_formatter = logging.Formatter(
        "[%(levelname)s] %(message)s " "{%(module)s:%(funcName)s:%(lineno)d}",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    console_handler.setFormatter(console_formatter)

    # add the handlers to the logger
    log.handlers = []
    log.addHandler(console_handler)

    # The Requests library (which uses urllib3) has very verbose logging in DEBUG
    # This makes it difficult to review log file (when set to DEBUG) and isn't overly useful.
    # Setting to only see WARNING, ERROR, and CRITICAL.
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)


# create logger - this is executed on file import
log = logging.getLogger()
log.setLevel(logging.DEBUG)
