"""GitLab interface."""

import functools
import os

import gitlab
import pandas as pd

from local_envvars import set_local

set_local()

GITLAB_URL = "https://gitlab.com/"
TOKEN = os.environ["GITLAB_TOKEN"]


gl = gitlab.Gitlab(GITLAB_URL, private_token=TOKEN)


def create_df_from_attrs(l: list) -> pd.DataFrame:
    """Convert an iterable of gitlab-pythons info a DataFrame.

    Args:
        l (list): Iterable from GitLab

    Returns:
        pd.DataFrame: DataFrame of l's content
    """
    return pd.DataFrame([i.__dict__["_attrs"] for i in l])


@functools.lru_cache(maxsize=32)
def get_wiki_page(project_id: int, page_slug: str) -> str:
    """Get contents of a GitLab wiki page.

    Args:
        project_id (int): Project ID for project containing Wiki
        page_slug (str): Page slug for the Wiki page

    Returns:
        str: Contents of Wiki page
    """
    return gl.projects.get(project_id, lazy=True).wikis.get(page_slug).content


@functools.lru_cache(maxsize=32)
def get_subgroups_with_details(group_id: int) -> pd.DataFrame:
    """Get group's subgroups along with associated details.

    Args:
        group_id (int): ID for parent group

    Returns:
        pd.DataFrame: Details of group's subgroups
    """
    subgroups_list = gl.groups.get(group_id).subgroups.list()
    return create_df_from_attrs(subgroups_list)


@functools.lru_cache(maxsize=32)
def get_projects(group_id: int) -> pd.DataFrame:
    """Get a list of projects for a given group.

    Args:
        group_id (int): ID for group

    Returns:
        pd.DataFrame: DataFrame of projects
    """
    p_list = gl.groups.get(group_id, lazy=True).projects.list()
    return create_df_from_attrs(p_list)


def get_packages(project_id: int, package_type: str = None) -> pd.DataFrame:
    """Get a list of packages for the given project.

    Args:
        project_id (int): Project ID of project.
        package_type (str): Type of package. Default is None.

    Returns:
        pd.DataFrame: Package list as DataFrame
    """
    packages = gl.projects.get(project_id, lazy=True).packages.list(
        package_type=package_type
    )
    return create_df_from_attrs(packages)


def get_merge_requests(project_id: int) -> pd.DataFrame:
    """Get Merge Requests for given project.

    Args:
        project_id (int): Project ID of project

    Returns:
        pd.DataFrame: List of MRs as DataFrame
    """
    merge_requests = gl.projects.get(project_id, lazy=True).mergerequests.list()
    return create_df_from_attrs(merge_requests)
