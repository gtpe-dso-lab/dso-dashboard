"""Constants file."""

DSO_LAB_GROUP_ID = 11186247  # https://gitlab.com/gtpe-dso-lab

# NOTE: COURSE_SUB_GROUP_ID & WIKI_PROJECT_ID need to change for each course
COURSE_SUB_GROUP_ID = 14297527  # https://gitlab.com/gtpe-dso-lab/2021-12
WIKI_PROJECT_ID = 31741343  # https://gitlab.com/gtpe-dso-lab/2021-12/wiki

UPSTREAM_DSO_DEPLOYMENT_PROJECT_ID = (
    21377323  # https://gitlab.com/gtpe-dso-lab/dso-deployment
)
DSO_DEPLOYMENT_URL = "https://gitlab.com/gtpe-dso-lab/dso-deployment/-/tree/main"
