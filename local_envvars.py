"""Set env vars when local (outside of CI)."""

import os

import yaml


def set_local():
    """When running locally (not in CI), set environmental variables."""
    local = not (os.environ.get("GITLAB_CI") or os.environ.get("HEROKU"))

    if local:
        creds = yaml.safe_load(open("credentials.yml", "r"))

        os.environ["HEROKU_APP_ID"] = creds.get("heroku", {}).get("app")
        os.environ["HEROKU_API_KEY"] = creds.get("heroku", {}).get("api_key")
        os.environ["GITLAB_TOKEN"] = creds.get("gitlab", {}).get("token")
        os.environ["GITLAB_WEBHOOK_TOKEN"] = creds.get("gitlab", {}).get("hook_token")


if __name__ == "__main__":
    set_local()
    for var in ("HEROKU_APP_ID", "HEROKU_API_KEY", "GITLAB_TOKEN", "GITLAB_WEBHOOK_TOKEN"):
        if not os.environ.get(var):
            print(f"{var} does not exist")
