# run linters
echo "isort"
isort .
echo "black"
black .
echo "mypy linting"
mypy .
echo "pylint linting"
pylint /
echo "pydocstyle linting"
pydocstyle
echo "linting complete"
