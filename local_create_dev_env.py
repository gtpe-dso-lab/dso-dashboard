"""Create python environment locally."""

import os
import subprocess

PYTHON_ENV_PATH = ".env"
PIP_PATH = f"{PYTHON_ENV_PATH}/bin/pip"


def create_environment():
    """Create python environment if it does not exist."""
    if True or not os.path.exists(PYTHON_ENV_PATH):
        subprocess.check_call(
            ["conda", "create", "-y", "-q", "-p", PYTHON_ENV_PATH, "python==3.9", "pip"]
        )
        subprocess.check_call([PIP_PATH, "install", "-q", "-r", "requirements.txt"])
        print("-" * 30)
        print("env environment created")
        print(f"to active environment run `conda activate ./{PYTHON_ENV_PATH}`")
    else:
        print(f"Python environment already exists in '{PYTHON_ENV_PATH}'")


if __name__ == "__main__":
    create_environment()
