"""DSO Dashboard Dash App."""

import logging
from datetime import datetime as dt
import os

import dash
from dash import dash_table, dcc, html
from dash.dependencies import Input, Output
from dash.development.base_component import Component
from flask import request, abort, jsonify

import logger
from dashboard import student_data, update_data_dso_deploy_mr, update_data_dso_deploy_folder
from local_envvars import set_local

set_local()
dcc.Graph.responsive = True

logger.create_logger()
log = logging.getLogger()

external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = "DSO Dashboard"
server = app.server

log.info("Server created")

log.debug("Creating DataFrame")
start_time = dt.now()

# global variable
df = student_data()

end_time = dt.now()
d_time = (end_time - start_time).total_seconds()
log.debug("Table created. %f seconds", d_time)


@app.callback(
    Output("data-table", "children"),
    Input('dashboard-update-interval', 'n_intervals'),
)
def generate_table(_) -> Component:
    """Generate dashboard table.

    Args:
        go_button (int): Number of clicks on go button.

    Returns:
        Component: Dash table for dashboard
    """
    return [
        dash_table.DataTable(
            id="student-table",
            data=df.to_dict("records"),
            columns=[{"name": i, "id": i} for i in df.columns],
            sort_action="native",
            style_cell={
                "font-family": "Open Sans",
                "font_size": "13px",
                "padding": "5px",
                "textAlign": "center",
                "width": f"{100/len(df.columns)}%",
            },
            style_table={
                "height": 400,
            },
            style_data_conditional=[
                {
                    "if": {"row_index": "odd"},
                    "backgroundColor": "rgb(248, 248, 248)",
                }
            ],
            style_header={
                "backgroundColor": "rgb(230, 230, 230)",
                "fontWeight": "bold",
            },
        ),
        html.Br(),
        html.Br(),
        html.P(f"Load time: {d_time:.1f}s"),
    ]


app.layout = html.Div(
    [
        dcc.Markdown("# DevSecOps Course Dashboard"),
        html.Hr(),
        dcc.Loading(html.Div("", id="data-table")),
        dcc.Interval(
            id='dashboard-update-interval',
            interval=5*1000, # in milliseconds
            n_intervals=0
        )
    ]
)


def validate_gitlab_webhook(header):
    if header.get('X-Gitlab-Token') == os.environ["GITLAB_WEBHOOK_TOKEN"]:
        return True
    abort(401)


def process_dso_deploy_webhook(payload):
    # https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#merge-request-events
    global df
    mr_action = payload.get("object_attributes", {}).get("action")
    if mr_action == "open":
        df = update_data_dso_deploy_mr(df)
    elif mr_action == "merge":
        df = update_data_dso_deploy_folder(df)
    # other options: close, reopen, update, approved, unapproved, merge


@server.route("/dsod-hook", methods=["POST"])
def dso_deploy_webhook():
    if request.method == 'POST':
        validate_gitlab_webhook(request.headers)
        log.info(request.json)
        if request.json.get("event_type") == "merge_request":
            process_dso_deploy_webhook(request.json)
            return jsonify({'status':'success'}), 200
    else:
        abort(400)


if __name__ == "__main__":
    app.run_server(debug=True, dev_tools_hot_reload=False)
