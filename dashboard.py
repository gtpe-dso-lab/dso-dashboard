"""Create dashboard-ready data."""

import functools
import logging
from typing import Union

import numpy as np
import pandas as pd
import requests
from requests.adapters import HTTPAdapter
from requests.exceptions import ConnectionError as RequestsConnectionError
from urllib3.util import Retry

from constants import (
    COURSE_SUB_GROUP_ID,
    DSO_DEPLOYMENT_URL,
    UPSTREAM_DSO_DEPLOYMENT_PROJECT_ID,
    WIKI_PROJECT_ID,
)
from gitlab_api import (
    get_merge_requests,
    get_packages,
    get_projects,
    get_subgroups_with_details,
    get_wiki_page,
)

log = logging.getLogger()


def stoplight_from_bool(s: Union[pd.Series, bool]) -> Union[pd.Series, str]:
    """Return boolean mapped to stoplight or series with booleans mapped to stoplights.

    Args:
        s (bool or pd.Series): Input boolean or boolean Series

    Returns:
        str or pd.Series: Output stoplight or Series with stoplights
    """
    return np.where(s, "🟢", "🔴")


def red_yellow_green(s: pd.Series) -> pd.Series:
    """Return series with value mapped to red, yellow, green.

    Args:
        s (pd.Series): Input boolean Series

    Returns:
        pd.Series: Output Series with stoplights
    """
    mapper = {
        "r": "🔴",
        "y": "🟡",
        "g": "🟢",
    }
    return s.map(mapper)


def qty_icon_mapper(s: pd.Series) -> pd.Series:
    """Return series with int mapped to icons.

    Args:
        s (pd.Series): Input Series

    Returns:
        pd.Series: Output Series with icons
    """
    mapper = {
        0: "🔴",
        1: "1️⃣",
        2: "2️⃣",
        3: "3️⃣",
        4: "4️⃣",
        5: "5️⃣",
        6: "6️⃣",
        7: "7️⃣",
        8: "8️⃣",
        9: "9️⃣",
        10: "🔟",
    }
    return s.clip(upper=max(mapper.keys())).map(mapper)


def ping(url: str) -> bool:
    """Ping URL and return True if ping is successful.

    Args:
        url (str): URL to ping

    Returns:
        bool: True if ping was successful, False otherwise.
    """
    log.debug("Pinging %s", url)
    session = requests.Session()
    retry = Retry(connect=2, backoff_factor=1)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount("http://", adapter)
    session.mount("https://", adapter)

    if not url.startswith("http"):
        url = f"http://{url}"

    try:
        r = session.get(url, verify=False, timeout=1)
    except RequestsConnectionError:
        return False

    return r.ok


def get_student_table() -> pd.DataFrame:
    """Get the table of students from wiki page.

    Returns:
        pd.DataFrame: Table as DataFrame.
    """

    def _extract_ips(student_id: str):
        df = student_table_df[student_table_df["Student Number"] == student_id]
        ip_addy_prod = df.loc[
            df["Endpoint"].str.contains("prod"),
            "IP Address",
        ]
        if len(ip_addy_prod) != 1:
            print(f"Issue extracting prod IP for Student Number {student_id}")
            return 0, 0
        prod = ip_addy_prod.values[0]
        ip_addy_stage = df.loc[
            df["Endpoint"].str.contains("staging"),
            "IP Address",
        ]
        if len(ip_addy_stage) != 1:
            print(f"Issue extracting staging IP for Student Number {student_id}")
            return 0, 0
        staging = ip_addy_stage.values[0]
        return staging, prod

    log.debug("Getting wiki page of IPs")
    student_table = get_wiki_page(WIKI_PROJECT_ID, "IPs")
    student_table_df = pd.DataFrame([s.split("|") for s in student_table.split("\n")])
    student_table_df = student_table_df.applymap(lambda x: x.strip())
    student_table_df.columns = student_table_df.loc[0, :]
    student_table_df = student_table_df.drop(index=[0, 1])

    student_table_df["Staging IP"], student_table_df["Prod IP"] = zip(
        *student_table_df["Student Number"].apply(_extract_ips)
    )

    student_table_df = student_table_df[
        ["Student Number", "Student", "GitLab Username", "Staging IP", "Prod IP"]
    ].drop_duplicates()
    return student_table_df


def get_student_subgroups() -> pd.DataFrame:
    """Get listing of existing student subgroups.

    Returns:
        pd.DataFrame: Student subgroups as DataFrame
    """
    log.debug("Getting student's subgroups")
    student_subgroups = get_subgroups_with_details(COURSE_SUB_GROUP_ID)
    return student_subgroups


def flask_app_fork_check(subgroup_id: int) -> bool:
    """Determine if flask_helloworld exists for student.

    Args:
        subgroup_id (int): Subgroup ID for student.

    Returns:
        bool: True if flask_helloworld exists, False otherwise.
    """
    if subgroup_id == 0:
        return False
    log.debug("Check to see if Flask App fork exists for %i", subgroup_id)
    projects = get_projects(subgroup_id)
    if "flask_helloworld" in projects["name"].values:
        return True
    return False


def dso_deployment_check(subgroup_id: int) -> bool:
    """Determine if dso-deployment exists for student.

    Args:
        subgroup_id (int): Subgroup ID for student.

    Returns:
        bool: True if dso-deployment exists, False otherwise.
    """
    if subgroup_id == 0:
        return False
    log.debug("Check to see if DSO Deployment fork exists for %i", subgroup_id)
    projects = get_projects(subgroup_id)
    if "dso-deployment" in projects["name"].values:
        return True
    return False


def dso_deployment_mr_check(username: str) -> str:
    """Determine if dso-deployment MR has been created by student upstream.

    Args:
        username (int): Username for student.

    Returns:
        str: 'r', 'y', or 'g' depending on status
            'g' means there is a merged MR submitted by student (and no open MRs)
            'y' means there is an open MR
            'r' means there are not any open or merged MRs
    """
    log.debug("Check to see if DSO Deployment MR exists for %s", username)
    merge_requests = get_merge_requests(UPSTREAM_DSO_DEPLOYMENT_PROJECT_ID)
    author_columns = merge_requests["author"].apply(pd.Series)
    author_columns.columns = [c + "_author" for c in author_columns.columns]
    merge_requests = pd.concat([merge_requests, author_columns], axis=1)
    users_mrs = merge_requests.loc[
        merge_requests["username_author"] == username, "state"
    ]
    status = "r"  # start with assumption it is red
    if len(users_mrs) > 0:
        if "merged" in users_mrs.values:
            # if there is a merged MR then it is green
            status = "g"
        if "opened" in users_mrs.values:
            # if you have an open MR it is yellow (even if there is one merged)
            # assumption is that you are correcting the merged one
            status = "y"
    return status


def dso_deployment_folder_check(student_number: str) -> bool:
    """Determine if student folder has been created upstream.

    Args:
        student_number (str): Student number for student.

    Returns:
        bool: True if dso-deployment MR exists, False otherwise.
    """
    log.debug(
        "Check to see if DSO Deployment folder exists upstream for %s", student_number
    )
    r = requests.get(
        f"{DSO_DEPLOYMENT_URL}/flask-helloworld-{student_number}", allow_redirects=False
    )
    # Since redirects are disallowed status code of 302 is returned if the folder does not exist
    if r.status_code == 200:
        return True
    return False


def helloworld_staging_check(student_number: str) -> bool:
    """Determine if Flask Helloworld app has deployed to staging.

    Args:
        student_number (int): Student ID.

    Returns:
        bool: True if flask helloworld has deployed to staging, False otherwise.
    """
    log.debug("Check to see if Flask App Staging exists for %s", student_number)
    table = get_student_table()
    ip_address = table.loc[table["Student Number"] == student_number, "Staging IP"]
    if len(ip_address) > 0:
        ip_address = ip_address.values[0]
        return ping(ip_address)
    return False


def helloworld_prod_check(student_number: int) -> bool:
    """Determine if Flask Helloworld app has deployed to prof.

    Args:
        student_number (int): Student ID.

    Returns:
        bool: True if flask helloworld has deployed to prod, False otherwise.
    """
    log.debug("Check to see if Flask App Prod exists for %s", student_number)
    table = get_student_table()
    ip_address = table.loc[table["Student Number"] == student_number, "Prod IP"]
    if len(ip_address) > 0:
        ip_address = ip_address.values[0]
        return ping(ip_address)
    return False


def pymodule_check(subgroup_id: int) -> bool:
    """Determine if pymodule exists for student.

    Args:
        subgroup_id (int): Subgroup ID for student.

    Returns:
        bool: True if pymodule exists, False otherwise.
    """
    if subgroup_id == 0:
        return False
    log.debug("Check to see if PyModule fork exists for %i", subgroup_id)
    projects = get_projects(subgroup_id)
    if "pymodule" in projects["name"].values:
        return True
    return False


def pymodule_deploy_check(subgroup_id: int) -> int:
    """Determine if pymodule package exists for student.

    Args:
        subgroup_id (int): Subgroup ID for student.

    Returns:
        int: Number of deployed packges.
    """
    if subgroup_id == 0:
        return 0
    log.debug("Check to see if PyModule package exists for %i", subgroup_id)
    projects = get_projects(subgroup_id)
    if "pymodule" in projects["name"].values:
        project_id = projects.loc[projects["name"] == "pymodule", "id"].values.tolist()[
            0
        ]
        packages = get_packages(project_id, package_type="pypi")
        return len(packages)
    return 0


@functools.lru_cache(maxsize=32)
def student_data() -> pd.DataFrame:
    """Get student data.

    Returns:
        pd.DataFrame: Student data.
    """
    log.debug("Gathering student data")
    table = get_student_table()
    subgroups = get_student_subgroups()
    extra_subgroups = set(subgroups["path"]) - set(table["GitLab Username"])
    log.info(
        f"Student subgroups that do not match student list: %s", str(extra_subgroups)
    )
    matrix = table.merge(
        subgroups[["path", "id"]],
        left_on="GitLab Username",
        right_on="path",
        how="left",
    )
    matrix = matrix.rename(columns={"id": "Subgroup ID"}).drop(columns=["path"])
    matrix["Subgroup Created"] = stoplight_from_bool(~matrix["Subgroup ID"].isna())
    matrix["Subgroup ID"] = matrix["Subgroup ID"].fillna(0).map(int)

    log.debug("Gathering: Flask Fork")
    matrix["Flask Fork"] = stoplight_from_bool(
        matrix["Subgroup ID"].apply(flask_app_fork_check)
    )
    log.debug("Gathering: DSO Deployment Fork")
    matrix["DSO Deployment Fork"] = stoplight_from_bool(
        matrix["Subgroup ID"].apply(dso_deployment_check)
    )
    log.debug("Gathering: DSO Deployment MR")
    matrix["DSO Deployment MR"] = red_yellow_green(
        matrix["GitLab Username"].apply(dso_deployment_mr_check)
    )
    log.debug("Gathering: DSO Deployment Folder")
    matrix["DSO Deployment Folder"] = stoplight_from_bool(
        matrix["Student Number"].apply(dso_deployment_folder_check)
    )
    log.debug("Gathering: HelloWorld Staging")
    matrix["HelloWorld Staging"] = stoplight_from_bool(
        matrix["Subgroup ID"].apply(helloworld_staging_check)
    )
    log.debug("Gathering: HelloWorld Prod")
    matrix["HelloWorld Prod"] = stoplight_from_bool(
        matrix["Subgroup ID"].apply(helloworld_prod_check)
    )
    log.debug("Gathering: PyModule Fork")
    matrix["PyModule Fork"] = stoplight_from_bool(
        matrix["Subgroup ID"].apply(pymodule_check)
    )
    log.debug("Gathering: PyModule Deployed")
    matrix["PyModule Deployed"] = qty_icon_mapper(
        matrix["Subgroup ID"].apply(pymodule_deploy_check)
    )

    matrix = matrix.drop(columns=["Subgroup ID", "GitLab Username"])

    return matrix

def update_data_dso_deploy_mr(data: pd.DataFrame) -> pd.DataFrame:
    log.info("update_data_dso_deploy_mr")
    
    # THIS IS JUST A TEST
    data.loc[data["Student Number"] == 13, "HelloWorld Prod"] = stoplight_from_bool(True)

    #####

    return data

def update_data_dso_deploy_folder(data: pd.DataFrame) -> pd.DataFrame:
    log.info("update_data_dso_deploy_folder")
    return data


if __name__ == "__main__":
    dso_deployment_folder_check("01")
