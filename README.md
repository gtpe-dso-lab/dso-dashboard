# DevSecOps Lab Dashboard

This dashboard is intended to be used during the lab exercises in support of the DevSecOps and Military Applications (DEF 4400P) course through Georgia Tech Professional Education.

Deployed Application: https://dso-course-dashboard.herokuapp.com

## Dev

### Python Environment

The Heroku deployment requires a `requirements.txt` file. To support local development there is a 
script that will create a local environment with the right python installed and install the dependencies 
defined in `requirements.txt`. 

To create the environment locally
```
python local_create_dev_env.py
```

Once you run `local_create_dev_env.py` you can activate the enviroment by running
```
conda activate ./.env
```

### Credentials

Create a file `credentials.yml` in your root consisting of
```
heroku:
  app: dso-course-dashboard
  api_key: <heroku_api_key>
gitlab:
  token: <gitlab_token>
```

### Running Dash App Locally
- Run the app
  ```bash
  python app.py
  ```
- Open `http://127.0.0.1:8050` in your browser. 
